# KiCAD_LIBRARIES_mine

# Temporary files ignored:
*.000

*.bak

*.bck

*.kicad_pcb-bak

*.sch-bak

*~

_autosave-*

*.tmp

*-save.pro

*-save.kicad_pcb

fp-info-cache

# Folders ignored:
digikey-kicad-library

kicad_packages3D_generator

kicad-footprints

kicad-library-utils

kicad-packages3D

kicad-symbols
