#!/bin/bash
# Git KiCad library repos:
#
# The "install_prerequisites" step is the only "distro dependent" one.  Could modify
# that step for other linux distros.
# This script requires "git".  The package bzr-git is not up to the task.
# The first time you run with option --install-or-update that is the slowest, because
# git clone from github.com is slow.
# After that updates should run faster.

# There are two reasons why you might want to run this script:
#
# 1) You want to contribute to the KiCad library team maintained libraries and have yet to
#    discover or have chosen not to use the COW feature in the Github "Plugin Type".
#
# 2) You want to run with local pretty footprint libraries and not those remotely located
#    on https://github.com using Github plugin.  After running this script you should
#      a)  $ cp ~/kicad_sources/library-repos/kicad-library/template/fp-lib-table.for-pretty ~/$TABLE_DEST_DIR/fp-lib-table
#    and then
#      b) set your environment variable KISYSMOD to "~/kicad_sources/library-repos".
#         Edit /etc/profile.d/kicad.sh, then reboot.
#
#    This will use the KiCad plugin against the *.pretty dirs in that base dir.

#Auto download kicad-libraries, kicad-footprints, kicad-templates, kicad-packages3D and kicad-symbols.
#this also updates the proper Table files so it is reflected in eeschema and pcbnew.

#This was adapted from the scipt entitled kicad-libraries-repos.sh


# Set where the library repos will go, use a full path
WORKING_TREES=${WORKING_TREES:-/crypt-storage1/electronics/KiCad}

#TABLE_DEST_DIR=.config/kicad
TABLE_DEST_DIR=.config/kicadnightly









usage()
{
    echo ""
    echo " usage:"
    echo ""
    echo "./kicad-libraries-repos.sh <cmd>"
    echo "    where <cmd> is one of:"
    echo "      --install-prerequisites     (install command tools needed here, run once first.)"
    echo "      --install-or-update         (from github, into $WORKING_TREES/LIBRARIES/. )"
    echo "      --remove-all-libraries      (remove all libraries from $WORKING_TREES/LIBRARIES/. )"
    echo "      --remove-orphaned-libraries (remove local footprint libraries which have been deleted or renamed at github.)"
    echo "      --list-footprint-libraries  (show the full list of github footprint libraries.)"
    echo ""
    echo "	examples (with --install-prerequisites once first):"
    echo "	$ ./kicad-libraries-repos.sh --install-prerequisites"
    echo "	$ ./kicad-libraries-repos.sh --install-or-update"
}


install_prerequisites()
{
    # Find a package manager, PM
    PM=$( command -v yum || command -v apt-get )

    # assume all these Debian, Mint, Ubuntu systems have same prerequisites
    if [ "$(expr match "$PM" '.*\(apt-get\)')" == "apt-get" ]; then
        #echo "debian compatible system"
        sudo apt-get install git curl #sed


    # assume all yum systems have same prerequisites
    elif [ "$(expr match "$PM" '.*\(yum\)')" == "yum" ]; then
        #echo "red hat compatible system"
        # Note: if you find this list not to be accurate, please submit a patch:
        sudo yum install git curl #sed


    else
        echo
        echo "Incompatible System. Neither 'yum' nor 'apt-get' found. Not possible to"
        echo "continue. Please make sure to install git, curl, and cut before using this"
        echo "script."
        echo
        exit 1
    fi
}


rm_build_dir()
{
    local dir="$1"
    # this file is often created as root, so remove as root
    sudo rm "$dir/install_manifest.txt" 2> /dev/null
    rm -rf "$dir"
}


cmake_uninstall()
{
    # assume caller set the CWD, and is only telling us about it in $1
    local dir="$1"

    cwd=`pwd`
    if [ "$cwd" != "$dir" ]; then
        echo "missing dir $dir"
    elif [ ! -e install_manifest.txt  ]; then
        echo
        echo "Missing file $dir/install_manifest.txt."
    else
        echo "uninstalling from $dir"
        sudo make uninstall
        sudo rm install_manifest.txt
    fi
}


detect_pretty_repos()
{
    # Check for the correct option to enable extended regular expressions in
    # sed. This is '-r' for GNU sed and '-E' for (older) BSD-like sed, as on
    # Mac OSX.
#    if [ $(echo | sed -r '' &>/dev/null; echo $?) -eq 0 ]; then
#        SED_EREGEXP="-r"
#    elif [ $(echo | sed -E '' &>/dev/null; echo $?) -eq 0 ]; then
#        SED_EREGEXP="-E"
#    else
#        echo "Your sed command does not support extended regular expressions. Cannot continue."
#        exit 1
#    fi

    # Use github API to list repos for org KiCad, then subset the JSON reply for only
    # *.pretty repos in the "full_name" variable."/KiCad/kicad-footprints/tree/master/Valve.pretty"
 #   repo_footprints=`curl -s "https://github.com/KiCad/kicad-footprints" 2> /dev/null \
 #       | sed $SED_EREGEXP 's:.+ "full_name".*"/KiCad/kicad-footprints/tree/master/(.+\.pretty)",:\1:p;d'`

    #echo "FP_REPOS:$FP_REPOS"

    FP_REPOS=`curl -s 'https://gitlab.com/kicad/libraries/kicad-footprints' 2> /dev/null \
    | grep -oP '(?<=href=")[^"]+.pretty(?=")' | cut -d'/' -f6-`

    FP_REPOS_BAT=`curl -s 'https://gitlab.com/kicad/libraries/kicad-footprints' 2> /dev/null \
    | grep -oP '(?<=href=")[^"]+.pretty(?=")' | cut -d'/' -f6-`

	#FP_REPOS=`echo $FP_REPOS | tr " " "\n" | sort`

   #echo "repo_footprints sorted:$repo_footprints"
}





update_dirs()
{

    if [ ! -d "$WORKING_TREES" ]; then
        sudo mkdir -p "$WORKING_TREES/LIBRARIES"
        echo " mark $WORKING_TREES/LIBRARIES as owned by me"
        sudo chown -R `whoami` "$WORKING_TREES/LIBRARIES"
    fi

    cd $WORKING_TREES

    if [ ! -e "$WORKING_TREES/LIBRARIES" ]; then
        mkdir -p "$WORKING_TREES/LIBRARIES"
    fi

    if [ ! -d "$WORKING_TREES" ]; then
        sudo mkdir -p "$WORKING_TREES/PLUGINS"
        echo " mark $WORKING_TREES/PLUGINS as owned by me"
        sudo chown -R `whoami` "$WORKING_TREES/PLUGINS"
    fi

    cd $WORKING_TREES

    if [ ! -e "$WORKING_TREES/PLUGINS" ]; then
        mkdir -p "$WORKING_TREES/PLUGINS"
    fi
}








update_kicad()
{
    for repo_kicad in 5.99; do


        if [ ! -e "$WORKING_TREES/$repo_kicad" ]; then

            echo "installing $WORKING_TREES/$repo_kicad"
            git clone "https://gitlab.com/kicad/code/kicad.git" "$WORKING_TREES/$repo_kicad"
        else
            echo "updating $WORKING_TREES/$repo_kicad"
            cd "$WORKING_TREES/$repo_kicad"
            git pull
        fi
    done
}


update_kicad_packages3D_generator()
{
    for repo_packages3D_generator in kicad_packages3D_generator; do

    if [ ! -e "$WORKING_TREES/LIBRARIES/$repo_packages3D_generator" ]; then

            echo "installing $WORKING_TREES/LIBRARIES/$repo_packages3D_generator"
            git clone "https://gitlab.com/kicad/libraries/kicad-packages3D-generator.git" "$WORKING_TREES/LIBRARIES/$repo_packages3D_generator"
        else
            echo "updating $WORKING_TREES/LIBRARIES/$repo_packages3D_generator"
            cd "$WORKING_TREES/LIBRARIES/$repo_packages3D_generator"
            git pull
        fi
    done
}



update_3d()
{

      for repo_3d in kicad-packages3D; do

        if [ ! -e "$WORKING_TREES/LIBRARIES/$repo_3d" ]; then

           echo "installing $WORKING_TREES/LIBRARIES/$repo_3d"
          git clone "https://gitlab.com/kicad/libraries/kicad-packages3D.git" "$WORKING_TREES/LIBRARIES/$repo_3d"
       else
           echo "updating $WORKING_TREES/LIBRARIES/$repo_3d"
           cd "$WORKING_TREES/LIBRARIES/$repo_3d"

           git pull
       fi
  done
}

update_symbols()
{

      for repo_kicad_symbols in kicad-symbols; do

        if [ ! -e "$WORKING_TREES/LIBRARIES/$repo_kicad_symbols" ]; then

           echo "installing $WORKING_TREES/LIBRARIES/$repo_kicad_symbols"

          git clone "https://gitlab.com/kicad/libraries/kicad-symbols.git" "$WORKING_TREES/LIBRARIES/$repo_kicad_symbols"
       else
           echo "updating $WORKING_TREES/LIBRARIES/$repo_kicad_symbols"
           cd "$WORKING_TREES/LIBRARIES/$repo_kicad_symbols"

           git pull
       fi
  done
}


update_utils()
{
    for repo_utils in kicad-library-utils; do

        if [ ! -e "$WORKING_TREES/LIBRARIES/$repo_utils" ]; then

            echo "installing $WORKING_TREES/LIBRARIES/$repo_utils"

            git clone "https://gitlab.com/kicad/libraries/kicad-library-utils.git" "$WORKING_TREES/LIBRARIES/$repo_utils"
        else
            echo "updating $WORKING_TREES/LIBRARIES/$repo_utils"
            cd "$WORKING_TREES/LIBRARIES/$repo_utils"

            git pull
        fi
    done
}
update_footprints()
{
    for repo_footprints in kicad-footprints; do
         #echo "repo_footprints:$repo_footprints"

        if [ ! -e "$WORKING_TREES/LIBRARIES/$repo_footprints" ]; then

            echo "installing $WORKING_TREES/LIBRARIES/$repo_footprints"

            git clone "https://gitlab.com/kicad/libraries/kicad-footprints.git" "$WORKING_TREES/LIBRARIES/$repo_footprints"
        else
            echo "updating $WORKING_TREES/LIBRARIES/$repo_footprints"
            cd "$WORKING_TREES/LIBRARIES/$repo_footprints"

            git pull
        fi
    done
}


update_templates()
{
    for repo_templates in kicad-templates; do

        if [ ! -e "$WORKING_TREES/$repo_templates" ]; then

            echo "installing $WORKING_TREES/$repo_templates"

            git clone "https://gitlab.com/kicad/libraries/kicad-templates.git" "$WORKING_TREES/$repo_templates"
        else
            echo "updating $WORKING_TREES/$repo_templates"
            cd "$WORKING_TREES/$repo_templates"

            git pull
        fi
    done
}


update_doc()
{
    for repo_doc in kicad-dev-docs; do
        if [ ! -e "$WORKING_TREES/$repo_doc" ]; then

            echo "installing $WORKING_TREES/$repo_doc"
git clone "https://gitlab.com/kicad/services/kicad-dev-docs.git" "$WORKING_TREES/$repo_doc"
        else
            echo "updating $WORKING_TREES/$repo_doc"
            cd "$WORKING_TREES/$repo_doc"

            git pull
        fi
    done
}


update_kicad-action-scripts()
{
    for repo_kicad_action_scripts in kicad-action-scripts; do


        if [ ! -e "$WORKING_TREES/PLUGINS/$repo_kicad_action_scripts" ]; then

            echo "installing $WORKING_TREES/PLUGINS/$repo_kicad_action_scripts"

            git clone "https://github.com/jsreynaud/kicad-action-scripts.git" "$WORKING_TREES/PLUGINS/$repo_kicad_action_scripts"
        else
            echo "updating $WORKING_TREES/PLUGINS/$repo_kicad_action_scripts"
            cd "$WORKING_TREES/PLUGINS/$repo_kicad_action_scripts"

            git pull
        fi
    done
}


update_digikey-kicad-library()
{
    for repo_digikey_kicad_library in digikey-kicad-library; do


        if [ ! -e "$WORKING_TREES/LIBRARIES/$repo_digikey_kicad_library" ]; then

            echo "installing $WORKING_TREES/LIBRARIES/$repo_digikey_kicad_library"

            git clone "https://github.com/digikey/digikey-kicad-library.git" "$WORKING_TREES/LIBRARIES/$repo_digikey_kicad_library"
        else
            echo "updating $WORKING_TREES/LIBRARIES/$repo_digikey_kicad_library"
            cd "$WORKING_TREES/LIBRARIES/$repo_digikey_kicad_library"

            git pull
        fi
    done
}

update_plugins()
{
    for repo_plugins in InteractiveHtmlBom; do


        if [ ! -e "$WORKING_TREES/PLUGINS/$repo_plugins" ]; then

            echo "installing $WORKING_TREES/PLUGINS/$repo_plugins"

            git clone "https://github.com/openscopeproject/InteractiveHtmlBom.git" "$WORKING_TREES/PLUGINS/$repo_plugins"
        else
            echo "updating $WORKING_TREES/PLUGINS/$repo_plugins"
            cd "$WORKING_TREES/PLUGINS/$repo_plugins"

            git pull
        fi
    done
}

listcontains()
{
    local list=$1
    local item=$2
    local ret=1
    local OIFS=$IFS

    # omit the space character from internal field separator.
    IFS=$'\n'

    for word in $list; do
        if [ "$word" == "$item" ]; then
            ret=0
            break
        fi
    done

    IFS=$OIFS
    return $ret
}


remove_orphaned_libraries()
{
    cd $WORKING_TREES/LIBRARIES/kicad-footprints

    if [ $? -ne 0 ]; then
        echo "Directory $WORKING_TREES/LIBRARIES/kicad-footprints does not exist."
        echo "The option --remove-orphaned-libraries should be used only after you've run"
        echo "the --install-or-update at least once."
        exit 2
    fi

    detect_pretty_repos

    for mylib in *.pretty; do
        echo "checking local lib: $mylib"

        if ! listcontains "$repo_footprints" "$mylib"; then
            echo "Removing orphaned local library $WORKING_TREES/LIBRARIES/kicad-footprints/$mylib"
            rm -rf "$mylib"
        fi
    done
}


if [ $# -eq 1 -a "$1" == "--install-or-update" ]; then
    update_dirs
    update_symbols
    update_footprints
    update_3d
    update_templates
    update_doc
    update_plugins
    update_kicad_packages3D_generator
    update_kicad
    update_utils
    update_kicad-action-scripts
    update_digikey-kicad-library
    echo "Placing KiCad Symbol & Footprint Library Tables = fp-lib-table and sym-lib-table to /home/$USER/$TABLE_DEST_DIR"


    #export KICAD6_FOOTPRINT_DIR="/crypt-storage1/electronics/KiCad/LIBRARIES/kicad-footprints";
    #export KICAD6_SYMBOL_DIR="/crypt-storage1/electronics/KiCad/LIBRARIES/kicad-symbols";
    #export KICAD6_3DMODEL_DIR="/crypt-storage1/electronics/KiCad/LIBRARIES/kicad-packages3D";
    #export KICAD6_TEMPLATE_DIR="/crypt-storage1/electronics/KiCad/LIBRARIES/kicad-templates/Projects";

    #source /usr/share/kicad-nightly/kicad-nightly.env;

    #source /home/su_pyrow/.bashrc;

    #cp /crypt-storage1/electronics/KiCAD/LIBRARIES/kicad-library/template/fp-lib-table.for-pretty /home/$USER/$TABLE_DEST_DIR/fp-lib-table;
    #sed 's/KISYSMOD/KISYSMOD_OLD/g' /crypt-storage1/electronics/KiCAD/LIBRARIES/kicad-library/template/fp-lib-table.for-pretty >> /home/$USER/$TABLE_DEST_DIR/fp-lib-table;
    head -n -1 /crypt-storage1/electronics/KiCad/LIBRARIES/kicad-footprints/fp-lib-table > /home/$USER/$TABLE_DEST_DIR/fp-lib-table;
    #cat /crypt-storage1/electronics/KiCAD/LIBRARIES/kicad-footprints/fp-lib-table >> /home/$USER/$TABLE_DEST_DIR/fp-lib-table;
    #sort -du /home/$USER/$TABLE_DEST_DIR/fp-lib-table > /home/$USER/$TABLE_DEST_DIR/fp-lib-table2;
    #echo "$(tail -n +2 /home/$USER/$TABLE_DEST_DIR/fp-lib-table2)" > /home/$USER/$TABLE_DEST_DIR/fp-lib-table;
    echo '  (lib (name MyCustomParts)(type KiCad)(uri "$(KISYSMINE)/MyCustomParts.pretty")(options "")(descr "My Custom Component Footprints")))' >> /home/$USER/$TABLE_DEST_DIR/fp-lib-table;
    #rm /home/$USER/$TABLE_DEST_DIR/fp-lib-table2;


    #cp /crypt-storage1/electronics/KiCAD/LIBRARIES/kicad-symbols/sym-lib-table /home/$USER/$TABLE_DEST_DIR/sym-lib-table;
    #sed 's/KICAD_SYMBOL_DIR/KICAD_SYMBOL_DIR_OLD/g' /crypt-storage1/electronics/KiCAD/LIBRARIES/kicad-library/template/sym-lib-table >> /home/$USER/$TABLE_DEST_DIR/sym-lib-table;
    head -n -1 /crypt-storage1/electronics/KiCad/LIBRARIES/kicad-symbols/sym-lib-table > /home/$USER/$TABLE_DEST_DIR/sym-lib-table
    #cat /crypt-storage1/electronics/KiCAD/LIBRARIES/kicad-symbols/sym-lib-table >> /home/$USER/$TABLE_DEST_DIR/sym-lib-table;
    #sort -u /home/$USER/$TABLE_DEST_DIR/sym-lib-table > /home/$USER/$TABLE_DEST_DIR/sym-lib-table2;
    #echo "$(tail -n +2 /home/$USER/$TABLE_DEST_DIR/sym-lib-table2)" > /home/$USER/$TABLE_DEST_DIR/sym-lib-table;
    #sed -i '1 i\(sym_lib_table' /home/$USER/$TABLE_DEST_DIR/sym-lib-table;
    #sed -i '$ d' /home/$USER/$TABLE_DEST_DIR/sym-lib-table;
    echo '  (lib (name "MyCustomComponents")(type "KiCad")(uri "${KISYSMINE}/MyCustomLibrary/MyCustomComponents.kicad_sym")(options "")(descr "My Custom Schematic Symbols")))' >> /home/$USER/$TABLE_DEST_DIR/sym-lib-table;

    #rm /home/$USER/$TABLE_DEST_DIR/sym-lib-table2;
    echo "Done installing/updating KiCad Symbol and Footprint Library Tables"
    exit
fi


if [ $# -eq 1 -a "$1" == "--remove-orphaned-libraries" ]; then
    remove_orphaned_libraries
    exit
fi



if [ $# -eq 1 -a "$1" == "--install-prerequisites" ]; then
    install_prerequisites
    exit
fi

if [ $# -eq 1 -a "$1" == "--list-footprint-libraries" ]; then

    # use github API to get repos into PRETTY_REPOS var
    detect_pretty_repos

    #for repo_footprints in $FP_REPOS; do
    echo "$repo_footprints"
    #done

    exit
fi

# may re-direct this output to a disk file for Windows *.BAT file creation.
if [ $# -eq 1 -a "$1" == "--create-bat-file" ]; then


        echo "REM This file was created using kicad-libraries-repos.sh on linux." >KiCad-5-libraries.bat;
	echo "REM Run it from a directory you desire as the base for all libraries." >>KiCad-5-libraries.bat;

    # add the "schematic parts & 3D model" kicad-library to total
        echo "git clone https://github.com/KiCad/kicad-footprints" >>KiCad-5-libraries.bat;
        echo "git clone https://github.com/KiCad/kicad-symbols" >>KiCad-5-libraries.bat;
        echo "git clone https://github.com/KiCad/kicad-packages3D" >>KiCad-5-libraries.bat;
        echo "git clone https://github.com/KiCad/kicad-templates" >>KiCad-5-libraries.bat;



        exit
fi

usage
