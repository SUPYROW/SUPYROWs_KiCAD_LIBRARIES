PCBNEW-LibModule-V1  2020-12-10 22:59:42
# encoding utf-8
Units mm
$INDEX
HYC77TF09200
$EndINDEX
$MODULE HYC77TF09200
Po 0 0 0 15 5fd2a85e 00000000 ~~
Li HYC77TF09200
Cd HYC77-TF09-200-1
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 0.000 0.225 1.27 1.27 0 0.254 N V 21 N "J**"
T1 0.000 0.225 1.27 1.27 0 0.254 N I 21 N "HYC77TF09200"
DS -7.375 -6.375 7.375 -6.375 0.2 24
DS 7.375 -6.375 7.375 8.125 0.2 24
DS 7.375 8.125 -7.375 8.125 0.2 24
DS -7.375 8.125 -7.375 -6.375 0.2 24
DS -9.35 -8.675 9.35 -8.675 0.1 24
DS 9.35 -8.675 9.35 9.125 0.1 24
DS 9.35 9.125 -9.35 9.125 0.1 24
DS -9.35 9.125 -9.35 -8.675 0.1 24
DS 7.375 -5.225 7.375 1.775 0.1 21
DS -7.375 4.775 -7.375 8.125 0.1 21
DS -7.375 8.125 7.375 8.125 0.1 21
DS 7.375 8.125 7.375 4.775 0.1 21
DS -7.375 -5.225 -7.375 1.775 0.1 21
$PAD
Po 2.250 -6.875
Sh "1" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.150 -6.875
Sh "2" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.050 -6.875
Sh "3" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.050 -6.875
Sh "4" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -2.150 -6.875
Sh "5" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -3.250 -6.875
Sh "6" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.350 -6.875
Sh "7" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -5.450 -6.875
Sh "8" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -6.550 -6.875
Sh "9" R 0.700 1.600 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.950 4.125
Sh "MH1" C 1.500 1.500 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.050 4.125
Sh "MH2" C 1.500 1.500 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -7.750 -6.325
Sh "MP1" R 1.200 1.500 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -7.750 3.725
Sh "MP2" R 1.200 2.200 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.750 3.725
Sh "MP3" R 1.200 2.200 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 6.850 -5.875
Sh "MP4" R 1.500 1.600 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE HYC77TF09200
$EndLIBRARY
